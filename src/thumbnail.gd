extends Button

var url = ""
var title = ""
func _ready():

	pass
	
	
func load_thumbnail(url):

	# Create an HTTP request node and connect its completion signal.
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.request_completed.connect(_http_request_completed)
	
	# Perform the HTTP request. The URL below returns a PNG image as of writing.
	var error = http_request.request(url)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
 
# Called when the HTTP request is completed.
func _http_request_completed(result, response_code, headers, body):
	
	var image = Image.new()
	var error = image.load_jpg_from_buffer(body)
	if error != OK:
		push_error("Couldn't load the image.")
 
	
	var texture = ImageTexture.create_from_image(image)
	icon = texture
	
 


func _on_pressed():
	var output = get_tree().get_nodes_in_group("msg_box")[0]
	output.text = "Playing " + title
	var results = []
	await get_tree().create_timer(0.5).timeout
	#OS.execute("mpv", ["--ytdl-format=22", "--fs", url], results)
	OS.execute("ytplay", [url], results)
	#print(results[0])
	output.text = "Done Playing " + title


func _on_focus_entered():
	var scrollbox = get_tree().get_nodes_in_group("scrollbox")[0]
	scrollbox.set_v_scroll(position.y)
	modulate.a = 2


func _on_focus_exited():
	modulate.a = 1
