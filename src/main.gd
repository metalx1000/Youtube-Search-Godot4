extends Control

const thumbnails = preload("res://thumbnail.tscn")


@onready var list = $VBoxContainer/scroll/list
@onready var searchbox = $VBoxContainer/HBoxContainer/searchbox
@onready var msg = $VBoxContainer/msg 
@onready var scroll = $VBoxContainer/scroll

func _ready():
	searchbox.grab_focus()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	scroll.scroll_vertical
	pass

func clear_list():
	for item in get_tree().get_nodes_in_group("thumbnails"):
		list.remove_child(item)
		item.queue_free()

func _on_searchbox_text_submitted(new_text):
	msg.text = "Searching for '" + new_text +"'"
	clear_list()
	await get_tree().create_timer(0.5).timeout
	search()
	
func search():
	var results = []
	var q = searchbox.text.split(" ")
	OS.execute("ytsearch",q,results)
	results = results[0].split("\n")

	clear_list()
	for r in results:
		if ! "|" in r:
			break
			
		var items = r.split("|")
		var thumbnail = thumbnails.instantiate()
		thumbnail.url = items[2]
		thumbnail.title = items[0]
		list.add_child(thumbnail)
		thumbnail.text = items[0]
		thumbnail.load_thumbnail(items[3])
	
