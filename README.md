# Youtube-Search-Godot4

Copyright Kris Occhipinti 2023-05-09

(https://filmsbykris.com)

License GPLv3

youtube-dl or yt-dlp required

# Install
~~~
sudo wget "https://gitlab.com/metalx1000/Youtube-Search-Godot4/-/raw/master/bin/youtube_player.x86_64" -O "/usr/local/bin/youtube_player"
sudo chmod +x /usr/local/bin/youtube_player 

sudo wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -O /usr/local/bin/yt-dlp
sudo chmod a+rx /usr/local/bin/yt-dlp  # Make executable
sudo ln -s /usr/local/bin/yt-dlp /usr/local/bin/youtube-dl

sudo wget "https://gitlab.com/metalx1000/Youtube-Search-Godot4/-/raw/master/scripts/ytplay" -O /usr/local/bin/ytplay
sudo wget "https://gitlab.com/metalx1000/Youtube-Search-Godot4/-/raw/master/scripts/ytsearch" -O /usr/local/bin/ytsearch
sudo chmod +x /usr/local/bin/ytplay
sudo chmod +x /usr/local/bin/ytsearch
~~~
